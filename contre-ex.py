from collections import namedtuple

# This code is an appendix of our article "Self-stabilizing clock synchronization in dynamic networks".
# It provides a simulation of the SAP algorithm in the case presented in section 5.2 of our paper, that is
#    - a system composed of three nodes, denoted 0, 1 and 2 in the code,
#    - the communication graph is the alternation of a well-chosen pattern composed of four communication graphs G, H_j, H_k and I.
#    - the initial state is described by two integers M0 and c0.
#
# If the parameters M0, c0, P and g are carefully chosen, the resulting execution of SAP does not synchronize. 

# M0 should be any positive integer
M0 = 1

# P should be any integer, strictly greater than 2
P = 3

# g should be any non-decreasing, inflationary function
g = lambda x:x+1

# c0 should satisfy 1 <= c0 <= P * M0 - 2
c0 = 1

G   = ( (0, 1, 2), (1,  ), (2,  ) )
H_k = ( (0, 1, 2), (0, 1), (2,  ) )
H_j = ( (0, 1, 2), (1,  ), (0, 2) )
I   = ( (0,     ), (1,  ), (2,  ) )

# local state space
Loc_state = namedtuple('Loc_state', 'm c')

# initial global state
state = (Loc_state(M0, c0), Loc_state(M0, c0), Loc_state(M0, 0))

def incoming_messages(graph, state, node):
    messages = []
    for neighbour in range(3):
        if node in graph[neighbour]:
            messages.append(state[neighbour])
    return messages

def next_state(loc_state, messages):
    c = 1 + min((x.c for x in messages))
    c = c % ( P * loc_state.m )
    m = max((x.m for x in messages))
    if not is_synchronized(messages):
        m = g(m)
    return Loc_state(m, c)

def is_synchronized(list_loc_states):
    first_c = list_loc_states[0].c
    return all(map(lambda x: first_c % P == x.c % P, list_loc_states))

# phi_j and phi_k are two predicate on the global state.
# Their formal definition is given in our paper.
def phi_j(state):
    return (state[0].m == state[1].m >= state[2].m
        and state[0].c == state[1].c <= P * state[0].m - 2
        and state[0].c % P != 0 and state[2].c == 0)

def phi_k(state):
    return (state[0].m == state[2].m >= state[1].m
        and state[0].c == state[2].c <= P * state[0].m - 2
        and state[0].c % P != 0 and state[1].c == 0)

nb_round = 0
while True:
    if is_synchronized(state):
        print('the system is synchronized in round', nb_round)
        break

    # picking a specific graph 
    if state[1].c == P * state[1].m - 2:
        graph = H_j
    elif state[2].c == P * state[2].m - 2:
        graph = H_k
    elif state[1].c == P * state[1].m - 1:
        graph = I
    elif state[2].c == P * state[2].m - 1:
        graph = I
    else:
        graph = G

    # computing the next global state
    state = tuple(( next_state( state[x], incoming_messages(graph, state, x))
            for x in range(3)))

    # checking if phi_j or phi_k hold
    if phi_j(state):
        print('the proposition phi_j holds in round', nb_round,
                '=> the system is not synchronized')
    if phi_k(state):
        print('the proposition phi_k holds in round', nb_round,
                '=> the system is not synchronized')
    
    nb_round += 1
